import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const imageElement = createFighterImage(fighter);
  const modalElement = {
    title: `The winner is ${fighter.name.toUpperCase()}!`,
    bodyElement: imageElement,
    onClose: () => {
      location.reload();
    }
  };

  showModal(modalElement);
}