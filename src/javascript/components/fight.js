import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const firstArenaFighter = createArenaFighter(firstFighter, 1);

    const secondArenaFighter = createArenaFighter(secondFighter, 2);

    firstArenaFighter.restartCritPoints();
    secondArenaFighter.restartCritPoints();

    const pressedKeys = new Map();
    const keys = Object.values(controls).flat(2);

    document.addEventListener('keydown', (e) => {
      if (e.repeat || !keys.some(key => key === e.code)) return;

      pressedKeys.set(e.code, true);

      doAction(firstArenaFighter, secondArenaFighter, pressedKeys, e.code);

      if (firstArenaFighter.currentHealth <= 0) {
        resolve(secondFighter);
      } else if (secondArenaFighter.currentHealth <= 0) {
        resolve(firstFighter);
      }
    });

    document.addEventListener('keyup', (e) => {
      if (e.code === controls.PlayerOneBlock) {
        firstArenaFighter.setIsBlocking(false);
      }
      if (e.code === controls.PlayerTwoBlock) {
        secondArenaFighter.setIsBlocking(false);
      }
      pressedKeys.delete(e.code);
    });   
  });
}

function createArenaFighter(fighter, position) {

  return {
    ...fighter,
    position,
    currentHealth: fighter.health,
    currentCritPoints: 0,
    isBlocking: false,
    timerId: null,

    receiveDamage(value) {
      this.currentHealth -= value;

      const side = position == 1 ? 'left' : 'right';
      const healthIndicator = document.getElementById(`${side}-fighter-indicator`);

      const indicatorWidth = Math.max(0, (this.currentHealth * 100) / this.health);
      healthIndicator.style.width = `${indicatorWidth}%`;
    },

    setIsBlocking(value) {
      this.isBlocking = value;
    },

    doAttack(defender, damage) {
      if (damage > 0)
        defender.receiveDamage(damage);
    },

    doCritAttack(defender) {
      if (!this.isCanDoCrit()) return;

      this.restartCritPoints();
      defender.receiveDamage(this.attack * 2);
    },

    isCanDoCrit() {
      return this.currentCritPoints >= 10;
    },

    restartCritPoints() {
      this.currentCritPoints = 0;

      this.timerId = setInterval(() => {
        this.currentCritPoints++;

        const canDoCrit = this.isCanDoCrit();

        if (canDoCrit) {
          clearInterval(this.timerId);
        }
      }, 1000);
    },
  };
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = 1 + Math.random();
  const power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = 1 + Math.random();;
  const power = fighter.defense * dodgeChance;
  return power;
}

function doAction(firstFighter, secondFighter, keyMap, currentCode) {
  if (currentCode === controls.PlayerOneBlock) {
    firstFighter.setIsBlocking(true);
  }
  if (currentCode === controls.PlayerTwoBlock) {
    secondFighter.setIsBlocking(true);
  }
  if (currentCode === controls.PlayerOneAttack) {
    attack(firstFighter, secondFighter, keyMap);
    return;
  }
  if (currentCode === controls.PlayerTwoAttack) {
    attack(secondFighter, firstFighter, keyMap);
    return;
  }
  if (controls.PlayerOneCriticalHitCombination.every(code => keyMap.has(code))) {
    firstFighter.doCritAttack(secondFighter);
    return;
  }
  if (controls.PlayerTwoCriticalHitCombination.every(code => keyMap.has(code))) {
    secondFighter.doCritAttack(firstFighter);
  }
}

function attack(attacker, defender) {
  if (attacker.isBlocking) {
    return;
  }

  if (defender.isBlocking) {
    attacker.doAttack(defender, 0);
    return;
  }

  attacker.doAttack(defender, getDamage(attacker, defender));
}